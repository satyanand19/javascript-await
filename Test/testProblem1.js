const path = require('path');
const { createDirectory, createFile, deleteFile, deleteAllFiles } = require('../problem1');

async function runFileCreationAndDeletion() {
    try {
        let directoryCreadted = await createDirectory(path.join(__dirname, 'Data of Json files'));
        console.log(directoryCreadted);
        for (let index = 1; index <= 3; index++) {
            let filecreated = await createFile(path.join(__dirname, 'Data of Json files', `data${index}.json`));
            console.log(filecreated);
        }

        deleteAllFiles(path.join(__dirname, 'Data of Json files'), deleteFile);

    } catch (err) {
        console.log(err);
    }


}
runFileCreationAndDeletion();
