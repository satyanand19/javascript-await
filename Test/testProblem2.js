const { readFile, convertIntoUpperCase, convertIntoLowerCase, sortAndStore, deleteFile, deleteAllFiles } = require('../problem2')
const path = require('path');

async function runFileCreationAndDeletion() {
    try {
        let readData = await readFile(path.join(__dirname, 'lipsum.txt'));
        let upperCaseMessage = await convertIntoUpperCase(readData);
        console.log(upperCaseMessage);
        let lowerCaseMessage = await convertIntoLowerCase(path.join(__dirname, '../DataInUpperCase.txt'));
        console.log(lowerCaseMessage);
        let sortMessage = await sortAndStore(path.join(__dirname, '../DataInLowerCase.txt'));
        console.log(sortMessage);
        deleteAllFiles(path.join(__dirname, '../filenames.txt'), deleteFile);

    } catch (err) {
        console.log(err);
    }

}
runFileCreationAndDeletion();
