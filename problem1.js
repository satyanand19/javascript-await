const fs = require('fs');
const path = require('path');

function createDirectory(directoryPath) {
    return new Promise((resolve, reject) => {
        fs.mkdir(directoryPath, (err) => {
            if (err) {
                reject(err)
            } else {
                resolve('Directory Created');
            }

        });
    });
}

function createFile(filePath) {
    return new Promise((resolve, reject) => {
        fs.writeFile(filePath, JSON.stringify({ a: 1, b: 2, c: 3 }), (err) => {
            if (err) {
                reject(err);
            } else {
                resolve('File has been saved');
            }
        });
    });
}

function deleteAllFiles(directoryPath, callback) {
    fs.readdir(directoryPath, (err, files) => {
        if (err) {
            console.log(err);
        } else {
            files.forEach(fileName => {
                callback(path.join(directoryPath, fileName))
                  .then(data => console.log(data))
                  .catch(err => console.log(err));
            });
        }
    });
}

function deleteFile(directoryPath) {
    return new Promise((resolve, reject) => {

        fs.unlink(directoryPath, (err) => {
            if (err) {
                reject(err);
            }
            else {
                resolve('File is deleted');
            }
        });
    });
}



module.exports = {
    createDirectory,
    createFile,
    deleteFile,
    deleteAllFiles
}