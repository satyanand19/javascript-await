// 1. Read the given file lipsum.txt
//         2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
//         3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
//         4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
//         5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.

const fs = require('fs');
const path = require('path');

function readFile(filePath) {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
}
function writeFile(filePath, data) {
    return new Promise((resolve, reject) => {
        fs.appendFile(filePath, data, (err) => {
            if (err) {
                reject(err + 'ABC');
            } else {
                resolve('File has been saved');
            }
        });
    });
}
async function convertIntoUpperCase(data) {

    try {
        let contentInUpperCase = data.toUpperCase();
        let message = await writeFile(path.join(__dirname, 'DataInUpperCase.txt'), contentInUpperCase);
        let fileMeassage = await writeFile(path.join(__dirname, 'filenames.txt'), 'DataInUpperCase.txt ');
        console.log(fileMeassage);
        return ("Converted into uppercase and " + message);

    } catch (err) {
        return (err);

    }

}


async function convertIntoLowerCase(filePath) {

    try {
        let data = await readFile(filePath);
        let lowerCaseSentenceArray = JSON.stringify(data.toLowerCase().split('.'));
        let message = await writeFile(path.join(__dirname, 'DataInLowerCase.txt'), lowerCaseSentenceArray);
        let filemessage = await writeFile(path.join(__dirname, 'filenames.txt'), 'DataInLowerCase.txt ');
        console.log(filemessage);
        return 'Converted into LowerCase Sentence' + message;
    } catch (err) {
        return err;
    }

}

async function sortAndStore(filePath) {

    try {
        let data = await readFile(filePath);
        let sortTheContent = (JSON.parse(data)).sort((a, b) => {
            return a > b ? 1 : -1;
        });
        let message = await writeFile(path.join(__dirname, 'sortedData.txt'), JSON.stringify(sortTheContent));
        let filemessage = await writeFile(path.join(__dirname, 'filenames.txt'), 'sortedData.txt ');
        console.log(filemessage);
        return 'Sorted the Content' + message;
    } catch (err) {
        return err;
    }

}

async function deleteAllFiles(filepath, callback) {
    try {
        let data = await readFile(filepath);
        let fileNames = data.split(' ');
        fileNames.pop();
        for (let fileName of fileNames) {
            let fileMessage = await callback(path.join(__dirname, fileName));
            console.log(fileMessage);
        }
    } catch (err) {
        console.log(err);
    }
}
function deleteFile(filePath) {
    return new Promise((resolve, reject) => {
        fs.unlink(filePath, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve('File is Deleted');
            }
        });
    });
}

module.exports = {
    readFile,
    convertIntoUpperCase,
    convertIntoLowerCase,
    sortAndStore,
    deleteFile,
    deleteAllFiles
}
